# Quickstart

1. Install docker if it is not already installed

        https://docs.docker.com/docker-for-mac/install/

2. Clone the repo

        git clone https://gitlab.com/gabelwright/blueboard.git

3. Navigate to the repo home directory (where `docker-compose.yml` is located)

4. Add a new text file called `debug-mode.env`

5. Copy the below text into it:

        SECRET_KEY=
        DB_PASSWORD=

        TOKEN_KEY=
        TOKEN_SALT=

        SETTINGS_LOCATION=blueboard.settings.debug

6. For the first four entries, add long random strings after the equal signs. For example:

        SECRET_KEY=jlUYTGJKYu6&RfgjhFGJdhgJGKGjfgghsxfgDH

7. Start docker with the below command.  The first time you start docker it will take awhile to download and build everything. After that it will go much quicker.

        docker-compose up

8. Navigate to http://localhost:8000/
