from django.urls import path
# from django.views.generic import TemplateView
from house.views import (IndexView, HomeView, LoginView, LogoutView, AddRecordView, EditRecordView,
                         EditTitleView, AddRoomView, RoomView, AddPurchaseView)
from . import views

app_name = 'house'
urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('house/', HomeView.as_view(), name='house'),
    path('room/add', AddRoomView.as_view(), name='add_room'),
    path('room/<room_id>/', RoomView.as_view(), name='room'),
    path('room/<room_id>/new/purchase', AddPurchaseView.as_view(), name='new_device'),
    path('record/add/', AddRecordView.as_view(), name='add_record'),
    path('record/delete/', views.delete_record, name='delete_record'),
    path('record/edit/', EditRecordView.as_view(), name='edit_record'),
    path('title/edit/', EditTitleView.as_view(), name='edit_title'),
]
