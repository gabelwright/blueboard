# Generated by Django 2.2.5 on 2019-09-14 01:47

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import house.models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='House',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created', models.DateTimeField(default=datetime.datetime(2019, 9, 13, 18, 47, 8, 751709))),
                ('name', models.CharField(max_length=50)),
                ('allow_stats', models.BooleanField(default=True)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Maintenance',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created', models.DateTimeField(default=datetime.datetime(2019, 9, 13, 18, 47, 8, 751709))),
                ('name', models.CharField(max_length=50)),
                ('date', models.DateField(blank=True, null=True)),
                ('warrenty_length', models.CharField(blank=True, max_length=150, null=True)),
                ('cost', models.PositiveIntegerField(blank=True, null=True)),
                ('receipt', models.FileField(blank=True, null=True, upload_to=house.models.user_directory_path)),
                ('notes', models.CharField(blank=True, max_length=1000, null=True)),
                ('frequency', models.CharField(blank=True, max_length=250, null=True)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Reminder',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created', models.DateTimeField(default=datetime.datetime(2019, 9, 13, 18, 47, 8, 751709))),
                ('remind_on', models.DateField()),
                ('email', models.EmailField(max_length=254)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created', models.DateTimeField(default=datetime.datetime(2019, 9, 13, 18, 47, 8, 751709))),
                ('name', models.CharField(max_length=50)),
                ('house', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='house.House')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Repair',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created', models.DateTimeField(default=datetime.datetime(2019, 9, 13, 18, 47, 8, 751709))),
                ('name', models.CharField(max_length=50)),
                ('date', models.DateField(blank=True, null=True)),
                ('warrenty_length', models.CharField(blank=True, max_length=150, null=True)),
                ('cost', models.PositiveIntegerField(blank=True, null=True)),
                ('receipt', models.FileField(blank=True, null=True, upload_to=house.models.user_directory_path)),
                ('company', models.CharField(blank=True, max_length=150, null=True)),
                ('contact', models.CharField(blank=True, max_length=250, null=True)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('reminder', models.ManyToManyField(blank=True, to='house.Reminder')),
                ('room', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='house.Room')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Record',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created', models.DateTimeField(default=datetime.datetime(2019, 9, 13, 18, 47, 8, 751709))),
                ('statement', models.CharField(max_length=150)),
                ('response', models.CharField(blank=True, max_length=150, null=True)),
                ('object_id', models.UUIDField()),
                ('content_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MaintenanceEvent',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created', models.DateTimeField(default=datetime.datetime(2019, 9, 13, 18, 47, 8, 751709))),
                ('name', models.CharField(max_length=50)),
                ('date', models.DateField(blank=True, null=True)),
                ('notes', models.CharField(blank=True, max_length=500, null=True)),
                ('maintenance', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='house.Maintenance')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('reminder', models.ManyToManyField(blank=True, to='house.Reminder')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='maintenance',
            name='reminder',
            field=models.ManyToManyField(blank=True, to='house.Reminder'),
        ),
        migrations.AddField(
            model_name='maintenance',
            name='room',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='house.Room'),
        ),
        migrations.CreateModel(
            name='Device',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created', models.DateTimeField(default=datetime.datetime(2019, 9, 13, 18, 47, 8, 751709))),
                ('name', models.CharField(max_length=50)),
                ('date', models.DateField(blank=True, null=True)),
                ('warrenty_length', models.CharField(blank=True, max_length=150, null=True)),
                ('cost', models.PositiveIntegerField(blank=True, null=True)),
                ('receipt', models.FileField(blank=True, null=True, upload_to=house.models.user_directory_path)),
                ('purchase_from', models.CharField(blank=True, max_length=150, null=True)),
                ('details', models.CharField(blank=True, max_length=150, null=True)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('reminder', models.ManyToManyField(blank=True, to='house.Reminder')),
                ('room', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='house.Room')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
