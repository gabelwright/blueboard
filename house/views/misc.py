from django.shortcuts import get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.views.generic.edit import FormView

from house.forms import EditTitleForm
from house.models import BaseName
from house.mixins import AjaxResponseMixin


class EditTitleView(AjaxResponseMixin, FormView):
    model = BaseName
    form_class = EditTitleForm

    def form_valid(self, form):
        print('did validate')
        try:
            object = get_object_or_404(
                form.cleaned_data['object_type'],
                pk=form.cleaned_data['object_id'],
                owner=self.request.user)
            object.name = form.cleaned_data['name']
            object.save()
            return HttpResponse(status=204)
        except:
            error_message = ['There was an error editing this name, please refresh the page and try again.']
            return JsonResponse({'error': error_message}, status=400)
