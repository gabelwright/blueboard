from django.shortcuts import get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import FormView

from house.forms import AddRecordForm, EditRecordForm
from house.models import Record
from house.mixins import AjaxResponseMixin


# TODO: Find way to add LoginRequiredMixin back without redirect
class EditRecordView(AjaxResponseMixin, FormView):
    model = Record
    form_class = EditRecordForm

    def form_valid(self, form):
        try:
            r = get_object_or_404(
                Record,
                pk=form.cleaned_data['record_id'],
                owner=self.request.user)
        except:
            error_message = ['There was an error editing this record, please refresh the page and try again.']
            return JsonResponse({'error': error_message}, status=400)
        r.statement = form.cleaned_data['statement']
        r.response = form.cleaned_data['response']
        r.save()
        return HttpResponse(status=204)


@login_required
def delete_record(request):
    if request.method == 'POST':
        record_id = request.POST.get('record_id')
        try:
            record = get_object_or_404(Record, owner=request.user, id=record_id)
            record.delete()
            return HttpResponse(status=204)
        except:
            error_message = [
                'There was an error deleting this record, please refresh the page and try again.']
            return JsonResponse({'error': error_message}, status=400)


class AddRecordView(AjaxResponseMixin, FormView):
    model = Record
    form_class = AddRecordForm

    def form_valid(self, form):
        try:
            obj = get_object_or_404(
                form.cleaned_data['object_type'],
                pk=form.cleaned_data['object_id'],
                owner=self.request.user)
        except:
            error_message = [
                'There was an error creating this record, please refresh the page and try again.']
            return JsonResponse({'error': error_message}, status=400)
        record = Record.objects.create(
            owner=self.request.user,
            statement=form.cleaned_data['statement'],
            content_object=obj
        )
        if form.cleaned_data.get('response'):
            record.response = form.cleaned_data['response']
        record.save()
        data = {'id': record.id}
        return JsonResponse(data)
