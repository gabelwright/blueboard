from django.shortcuts import render
from django.views.generic import FormView, RedirectView
from django.contrib.auth.models import User
from django.urls import reverse_lazy, reverse
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout

from house.forms import SignUpForm, AuthForm


class IndexView(FormView):
    template_name = 'house/page/index.html'
    form_class = SignUpForm
    success_url = reverse_lazy('house:house')

    def form_valid(self, form):
        form.create_user()
        user = User.objects.filter(email=form.cleaned_data['email']).first()
        login(self.request, user)
        return super().form_valid(form)


class LoginView(FormView):
    template_name = 'house/page/login.html'
    form_class = AuthForm
    success_url = reverse_lazy('house:house')

    def get(self, request):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse('house:house'))
        else:
            form = AuthForm
            return render(request, 'house/page/login.html', {'form': form})

    def form_valid(self, form):
        username = form.cleaned_data['email']
        if '@' in form.cleaned_data['email']:
            user = User.objects.filter(
                email=form.cleaned_data['email']).first()
            username = user.username
        user = authenticate(
            username=username,
            password=form.cleaned_data['password'])
        if user is None:
            messages.info(self.request, 'Invalid email/password')
            return render(self.request, 'house/page/login.html', {'form': form})
        else:
            login(self.request, user)
            return super().form_valid(form)


class LogoutView(RedirectView):
    url = reverse_lazy('house:login')

    def get(self, request):
        url = self.get_redirect_url()
        logout(request)
        messages.info(self.request, 'You have been logged out')
        return HttpResponseRedirect(url)
