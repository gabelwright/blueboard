from django.shortcuts import redirect
from django.views.generic import TemplateView, CreateView
from django.urls import reverse_lazy, reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.models import ContentType
from django.core import serializers

from house.models import House, Record, Room
from house.forms import AddRoomForm
from house.utils import get_records


class HomeView(LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy('house:login')
    template_name = 'house/page/house.html'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        house = House.objects.filter(owner=self.request.user).first()
        data['house'] = house
        records = get_records(self.request.user, house.id, 'house')
        data['records'] = records
        return data
