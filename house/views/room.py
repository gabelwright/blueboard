from django.shortcuts import redirect
from django.views.generic import TemplateView, FormView
from django.urls import reverse_lazy, reverse
from django.contrib.auth.mixins import LoginRequiredMixin
# from django.contrib.contenttypes.models import ContentType
# from django.contrib import messages

from house.models import House, Room, Repair, Device, Maintenance
from house.forms import AddRoomForm, AddDeviceForm
from house.utils import get_records

from itertools import chain


class RoomView(LoginRequiredMixin, TemplateView):
    template_name = 'house/page/room.html'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        room_id = self.kwargs.get('room_id')
        room = Room.objects.filter(owner=self.request.user, id=room_id).first()
        records = get_records(self.request.user, room.id, 'room')
        data['room'] = room
        data['records'] = records
        repairs = Repair.objects.filter(owner=self.request.user, room=room).order_by("date").all()
        devices = Device.objects.filter(owner=self.request.user, room=room).all()
        maintenance = Maintenance.objects.filter(owner=self.request.user, room=room).all()
        objects = list(chain(repairs, devices, maintenance))
        objects = sorted(objects, key=lambda o: o.date)
        data['objects'] = objects
        return data


class AddRoomView(LoginRequiredMixin, FormView):
    login_url = reverse_lazy('house:login')
    template_name = 'house/page/all_rooms.html'
    model = Room
    form_class = AddRoomForm

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        rooms = Room.objects.filter(owner=self.request.user).all()
        rooms.order_by('name')
        data['rooms'] = rooms
        return data

    def form_valid(self, form):
        name = form.cleaned_data['name']
        house = House.objects.filter(owner=self.request.user).first()
        r = Room.objects.create(name=name, house=house, owner=self.request.user)
        return redirect(reverse('house:room', args=[r.id]))


class AddPurchaseView(LoginRequiredMixin, FormView):
    login_url = reverse_lazy('house:login')
    template_name = 'house/page/new_device.html'
    model = Device
    form_class = AddDeviceForm

    def get_context_data(self, **kwargs):
        # TODO: handle case where no room is found
        data = super().get_context_data(**kwargs)
        room = Room.objects.filter(owner=self.request.user).filter(id=self.kwargs['room_id']).first()
        data['room'] = room
        return data

    def form_valid(self, form):
        print('valid form')
        room = Room.objects.filter(owner=self.request.user).filter(id=self.kwargs['room_id']).first()
        device = Device(name=form.cleaned_data['name'],
                        room=room,
                        date=form.cleaned_data['date'],
                        warrenty_length=form.cleaned_data['warrenty_length'],
                        cost=form.cleaned_data['cost'],
                        notes=form.cleaned_data['notes'],
                        owner=self.request.user)
        device.save()
        return redirect(reverse('house:room', args=[room.id]))

    def form_invalid(self, form):
        print('form invalid')
        print(form['date'])
        return super(AddPurchaseView, self).form_invalid(form)













#
