from django.test import TestCase
from django.test import Client
from house.models import House, Record
from django.urls import reverse_lazy
from django.contrib.contenttypes.models import ContentType

from house.tests.utils import create_user, get_house


class AddRecordsHouse(TestCase):
    def setUp(self):
        self.c = Client()
        self.statement = 'test statement'
        self.response = 'test response'
        self.user = create_user()
        self.house = get_house(self.user)
        self.data = {
            'statement': self.statement,
            'response': self.response,
            'object_type': 'house',
            'object_id': self.house.id
        }
        self.url = reverse_lazy('house:add_record')

    def test_add_record(self):
        self.c.force_login(self.user)
        response = self.c.post(self.url, self.data)
        # verify one record was created properly and it was attached to the right house
        ct = ContentType.objects.get_for_model(House)
        r = Record.objects.filter(
            statement=self.statement,
            response=self.response,
            owner=self.user,
            content_type=ct,
            object_id=self.house.id
        ).all()
        self.assertEqual(r.count(), 1)

    def test_no_user(self):
        response = self.c.post(self.url, self.data)
        self.assertEqual(response.status_code, 400)
        count = Record.objects.all().count()
        self.assertEqual(count, 0)

    def test_wrong_house(self):
        w_user = create_user()
        w_house = get_house(w_user)
        self.c.force_login(self.user)
        data = self.data
        data['object_id'] = w_house.id
        response = self.c.post(self.url, data)
        self.assertEqual(response.status_code, 400)
        count = Record.objects.all().count()
        self.assertEqual(count, 0)

    def test_wrong_object_type(self):
        self.c.force_login(self.user)
        data = self.data
        data['object_type'] = 'device'
        response = self.c.post(self.url, data)
        self.assertEqual(response.status_code, 400)

        count = Record.objects.all().count()
        self.assertEqual(count, 0)

    def test_invalid_object_type(self):
        self.c.force_login(self.user)
        data = self.data
        data['object_type'] = 'car'
        response = self.c.post(self.url, data)
        self.assertEqual(response.status_code, 400)

        count = Record.objects.all().count()
        self.assertEqual(count, 0)
