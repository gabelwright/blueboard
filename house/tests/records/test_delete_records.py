from django.test import TestCase
from django.test import Client
from house.models import Record
from django.urls import reverse_lazy, reverse

from house.tests.utils import create_user, get_house, create_record


class DeleteRecords(TestCase):
    def setUp(self):
        self.c = Client()
        self.user = create_user()
        self.house = get_house(self.user)
        self.record = create_record(self.user, self.house)
        self.url = reverse_lazy('house:delete_record')

    def test_record_delete(self):
        self.c.force_login(self.user)
        r = Record.objects.all().count()
        self.assertEqual(r, 1)
        response = self.c.post(self.url, {'record_id': self.record.id})
        self.assertEqual(response.status_code, 204)
        r = Record.objects.all().count()
        self.assertEqual(r, 0)

    def test_wrong_user(self):
        w_user = create_user()
        self.c.force_login(w_user)
        response = self.c.post(self.url, {'record_id': self.record.id})
        self.assertEqual(response.status_code, 400)
        r = Record.objects.all().count()
        self.assertEqual(r, 1)

    def test_no_user(self):
        response = self.c.post(self.url, {'record_id': self.record.id})
        redirect_url = reverse('house:login') + '?next=' + reverse('house:delete_record')
        self.assertRedirects(
            response,
            redirect_url,
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True)
        r = Record.objects.all().count()
        self.assertEqual(r, 1)
