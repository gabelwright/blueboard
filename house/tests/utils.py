from house.models import House, Record
from django.contrib.auth.models import User

import random
import string


def gen_string(length=10):
    return ''.join([random.choice(string.ascii_letters + string.digits) for n in range(length)])


def create_user(username=None, email=None, password=None):
    if not username:
        username = gen_string()
    if not email:
        email = '{}@blueboard.xyz'.format(gen_string())
    if not password:
        password = '12345678'
    user = User.objects.create_user(username=username,
                                    email=email,
                                    password=password)
    House.objects.create(owner=user, name='Your House')
    return user


def get_house(user):
    return House.objects.filter(owner=user).first()


def create_record(user, object, statement=None, response=None):
    if not statement:
        statement = gen_string()
    if not response:
        response = gen_string()
    record = Record.objects.create(statement=statement,
                                   response=response,
                                   owner=user,
                                   content_object=object)

    return record
