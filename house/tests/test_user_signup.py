from django.test import TestCase
from django.contrib.auth.models import User
from django.test import Client
from django.urls import reverse
from house.models import House


class UserSignUp(TestCase):
    def setUp(self):
        self.c = Client()

    def test_user_creation(self):
        email = 'hack@blueboard.xyz'
        response = self.c.post('/', {'email': email, 'password': '12345678'})
        self.assertRedirects(
            response,
            reverse('house:house'),
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True)
        count = User.objects.filter(email=email, username=email).count()

        self.assertEquals(count, 1)
        user = User.objects.filter(email=email, username=email).first()

        count = House.objects.filter(owner=user).count()
        self.assertEquals(count, 1)

    def test_bad_email(self):
        email = 'hack@blue'
        password = '12345678'
        response = self.c.post('/', {'email': email, 'password': password})
        self.assertEqual(response.status_code, 200)
        user = User.objects.filter(email=email).count()
        self.assertEquals(user, 0)

    def test_duplicate_email(self):
        email = 'hack@blueboard.xyz'

        response = self.c.post('/', {'email': email, 'password': 'password'})
        self.assertRedirects(
            response,
            reverse('house:house'),
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True)
        user = User.objects.filter(email=email).count()
        self.assertEquals(user, 1)

        response = self.c.post('/', {'email': email, 'password': 'password2'})
        self.assertEqual(response.status_code, 200)
        user = User.objects.filter(email=email).count()
        self.assertEquals(user, 1)

    def test_bad_password(self):
        email = 'hack@blueboard.xyz'
        response = self.c.post('/', {'email': email, 'password': '1234567'})
        self.assertEqual(response.status_code, 200)
        user = User.objects.filter(email=email).count()
        self.assertEquals(user, 0)
