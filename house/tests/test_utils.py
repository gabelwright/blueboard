from django.test import TestCase
from django.contrib.auth.models import User
from django.test import Client
from house.models import House, Record
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import authenticate

from house.tests.utils import create_user, get_house, gen_string, create_record


class Utils(TestCase):
    def setUp(self):
        self.c = Client()

    def test_gen_string(self):
        str1 = gen_string()
        str2 = gen_string()
        str3 = gen_string(15)

        self.assertEqual(len(str1), 10)
        self.assertEqual(len(str2), 10)
        self.assertNotEqual(str1, str2)
        self.assertEqual(len(str3), 15)

    def test_create_user_random(self):
        user = create_user()
        u = User.objects.all()
        self.assertEqual(u.count(), 1)
        u = u[0]
        self.assertEqual(user.id, u.id)
        count = House.objects.filter(owner=user).all().count()
        self.assertEqual(count, 1)

    def test_create_user(self):
        user = create_user(username='test_user',
                           email='test@test.com',
                           password='testpassw')
        u = User.objects.filter(username='test_user',
                                email='test@test.com').all()
        self.assertEqual(u.count(), 1)
        u = u[0]
        self.assertEqual(user.id, u.id)
        login_user = authenticate(username=u.username, password='testpassw')
        self.assertIsNotNone(login_user)
        count = House.objects.filter(owner=user).all().count()
        self.assertEqual(count, 1)

    def test_get_house(self):
        user = User.objects.create(username='test_user',
                                   email='test@test.com',
                                   password='testpassw')
        house = House.objects.create(owner=user,
                                     name='test house')
        h = get_house(user)
        self.assertEqual(h.id, house.id)

    def test_create_record_random(self):
        user = User.objects.create(username='test_user',
                                   email='test@test.com',
                                   password='testpassw')
        house = House.objects.create(owner=user,
                                     name='test house')
        record = create_record(user, house)
        r = Record.objects.all()
        self.assertEqual(r.count(), 1)
        r = r[0]
        self.assertEqual(r.owner.id, user.id)
        self.assertEqual(r.object_id, house.id)
        ct = ContentType.objects.get_for_model(House)
        self.assertEqual(r.content_type, ct)

    def test_create_record(self):
        user = User.objects.create(username='test_user',
                                   email='test@test.com',
                                   password='testpassw')
        house = House.objects.create(owner=user,
                                     name='test house')
        record = create_record(user,
                               house,
                               statement='test statement',
                               response='test response')
        r = Record.objects.all()
        self.assertEqual(r.count(), 1)
        r = r[0]
        self.assertEqual(r.statement, 'test statement')
        self.assertEqual(r.response, 'test response')
        self.assertEqual(r.owner.id, user.id)
        self.assertEqual(r.object_id, house.id)
        ct = ContentType.objects.get_for_model(House)
        self.assertEqual(r.content_type, ct)

















##
