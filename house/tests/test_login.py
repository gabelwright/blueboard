from django.test import TestCase
from django.test import Client
from django.urls import reverse

from house.tests.utils import create_user, gen_string


class UserLogin(TestCase):
    def setUp(self):
        self.c = Client()
        self.email = '{}@blueboard.xyz'.format(gen_string())
        self.password = gen_string()
        self.username = gen_string()
        self.user = create_user(username=self.username,
                                email=self.email,
                                password=self.password)

    def test_login_by_username(self):
        response = self.c.post(
            '/login/',
            {'email': self.username, 'password': self.password})
        self.assertRedirects(
            response,
            reverse('house:house'),
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True)
        self.assertIn('_auth_user_id', self.c.session)
        self.assertEqual(self.user.id, int(self.c.session['_auth_user_id']))

    def test_login_by_email(self):
        response = self.c.post(
            '/login/',
            {'email': self.email, 'password': self.password})
        self.assertRedirects(
            response,
            reverse('house:house'),
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True)
        self.assertIn('_auth_user_id', self.c.session)
        self.assertEqual(self.user.id, int(self.c.session['_auth_user_id']))

    def test_login_wrong_email(self):
        response = self.c.post(
            '/login/',
            {'email': gen_string(), 'password': gen_string()})
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('_auth_user_id', self.c.session)

    def test_login_wrong_password(self):
        response = self.c.post(
            '/login/',
            {'email': self.username, 'password': gen_string()})
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('_auth_user_id', self.c.session)

    def test_user_logout(self):
        self.c.force_login(self.user, backend=None)
        response = self.c.get('/logout/')
        self.assertRedirects(
            response,
            reverse('house:login'),
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True)
        self.assertNotIn('_auth_user_id', self.c.session)
