from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

import datetime
import uuid


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_{0}/{1}'.format(instance.user.id, filename)


class Base(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(default=datetime.datetime.now())

    class Meta:
        abstract = True


class Reminder(Base):
    remind_on = models.DateField()
    email = models.EmailField()


class BaseName(Base):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name + ' - ' + str(self.owner)

    class Meta:
        abstract = True


class House(BaseName):
    allow_stats = models.BooleanField(default=True)


class Room(BaseName):
    house = models.ForeignKey(House, on_delete=models.CASCADE)

    def __str__(self):
        return self.name + ' - ' + str(self.owner)


class BaseObject(BaseName):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    date = models.DateField(blank=True, null=True)
    warrenty_length = models.CharField(max_length=150, blank=True, null=True)
    cost = models.PositiveIntegerField(blank=True, null=True)
    receipt = models.FileField(upload_to=user_directory_path, blank=True, null=True)
    reminder = models.ManyToManyField(Reminder, blank=True)
    notes = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        abstract = True


class Maintenance(BaseObject):
    frequency = models.CharField(max_length=250, blank=True, null=True)


class MaintenanceEvent(BaseName):
    maintenance = models.ForeignKey(Maintenance, on_delete=models.CASCADE)
    date = models.DateField(blank=True, null=True)
    notes = models.CharField(max_length=500, blank=True, null=True)
    reminder = models.ManyToManyField(Reminder, blank=True)


class Device(BaseObject):
    purchase_from = models.CharField(max_length=150, blank=True, null=True)
    details = models.CharField(max_length=150, blank=True, null=True)


class Repair(BaseObject):
    company = models.CharField(max_length=150, blank=True, null=True)
    contact = models.CharField(max_length=250, blank=True, null=True)


class Record(Base):
    statement = models.CharField(max_length=150)
    response = models.CharField(max_length=150, blank=True, null=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.UUIDField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        ordering = ['-created']
