from django.http import JsonResponse


class AjaxResponseMixin:
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """
    def form_invalid(self, form):
        error_message = []
        for error in form.errors:
            error_message.append(form.errors[error].as_text())
        return JsonResponse({'error': error_message}, status=400)

    def form_valid(self, form):
        response = super().form_valid(form)
        if self.request.is_ajax():
            data = {
                'pk': self.object.pk,
            }
            return JsonResponse(data)
        else:
            return response
