from django.contrib.contenttypes.models import ContentType
from django.core import serializers

from house.models import Record, Room, House, Repair, Device,  Maintenance, MaintenanceEvent


def get_object_from_type(value):
    object_types = {
        'house': House,
        'room': Room,
        'device': Device,
        'repair': Repair,
        'maintenance': Maintenance,
        'maintenanceEvent': MaintenanceEvent
    }
    return object_types.get(value)


def get_records(user, id, object_type):
    obj = get_object_from_type(object_type)
    ct = ContentType.objects.get_for_model(obj)
    records = Record.objects.filter(
        owner=user, content_type=ct, object_id=id).order_by('-created').all()
    records = serializers.serialize(
        'json', records, ensure_ascii=False, fields=('statement', 'response', 'id'))
    return records
