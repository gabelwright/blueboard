from django.contrib import admin
from house.models import Record, House, Room, Maintenance, MaintenanceEvent, Device, Repair


@admin.register(Record)
class RecordAdmin(admin.ModelAdmin):
    model = Record

    list_display = ['owner', 'statement', 'response']


@admin.register(House)
class HouseAdmin(admin.ModelAdmin):
    model = House

    list_display = ['owner', 'name']


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    model = Room

    list_display = ['owner', 'name', 'house']


@admin.register(Maintenance)
class MaintenanceAdmin(admin.ModelAdmin):
    model = Maintenance

    list_display = ['owner', 'name', 'room', 'frequency']


@admin.register(MaintenanceEvent)
class MaintenanceEventAdmin(admin.ModelAdmin):
    model = MaintenanceEvent

    list_display = ['owner', 'maintenance', 'date']


@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    model = Device

    list_display = ['owner', 'name', 'room', 'purchase_from']


@admin.register(Repair)
class RepairAdmin(admin.ModelAdmin):
    model = Repair

    list_display = ['owner', 'name', 'room', 'company']
