function build_table(records){
    let data = JSON.parse(records);
    let data_reverse = data.slice(0).reverse();
    for(record of data_reverse){
        let statement = record.fields.statement;
        let response = record.fields.response;
        let id = record.pk;
        createRow(statement, response, id);
    }
}

function add_record(id, type){
    $('#add-record').off();
    $('#add-record-statement').val();
    $('#add-record-response').val();

    $('#add-record').click(function(){
        let statement = $('#add-record-statement').val();
        let response = $('#add-record-response').val();
        let csrftoken = getToken();

        data = {
            'statement': statement,
            'response': response,
            'object_id': id,
            'object_type': type
        }
        $.ajax({
            headers: { "X-CSRFToken": csrftoken },
            type: "POST",
            url: '/record/add/',
            data: data,
            dataType: 'json',
            success: function(xhr){
                let cell = createRow(statement, response, xhr['id']);
                $('#record-table-tbody').prepend(cell);
                $('#add-record-statement').val('');
                $('#add-record-response').val('');
                $('#add-record-modal').modal('hide');
            },
            error: function(xhr){
                errorResponseTitle(xhr, 'add-record')
            }
        })
    });
    $('#add-record-modal').modal('show');
}

function edit_record(id, statement, response=''){
    $('#add-record').off();
    $('#add-record-statement').val(statement);
    $('#add-record-response').val(response);

    $('#add-record').click(function(){
        let new_statement = $('#add-record-statement').val();
        let new_response = $('#add-record-response').val();

        var csrftoken = getToken();
        data = {
            'record_id': id,
            'statement': new_statement,
            'response': new_response
        }
        $.ajax({
            headers: { "X-CSRFToken": csrftoken },
            type: "POST",
            url: '/record/edit/',
            data: data,
            dataType: 'json',
            success: function(){
                $('#add-record-modal').modal('hide');
                $(`#statement-${id}`).html(new_statement);
                $(`#response-${id}`).html(new_response);
            },
            error: function(xhr, textStatus, errorThrown){
                errorResponseTitle(xhr, 'add-record')
            }
        })
    });

    $('#add-record-modal').modal('show');
}

function delete_record(id, statement){
    $('#confirm-delete-record').off();
    $('#deleteModalBody').text(statement);
    $('#confirm-delete-record').click(function(){
        var csrftoken = getToken();
        data = {'record_id': id}
        $.ajax({
            headers: { "X-CSRFToken": csrftoken },
            type: "POST",
            url: '/record/delete/',
            data: data,
            dataType: 'json',
            success: function(){
                $('#deleteModal').modal('hide');
                $(`#row-${id}`).remove();
            },
            error: function(xhr, textStatus, errorThrown){
                errorResponseTitle(xhr, 'delete-record')
            }
        })
    });
    $('#deleteModal').modal('show');
}

function createRow(statement, response, id){
    let $row = $('<tr>', {'id': `row-${id}`});
    let $sCell = $('<td>', {'class': 'record-table-cell', 'id': `statement-${id}`}).html(statement);
    let $rCell = $('<td>');

    let $div1 = $('<div>', {'class': 'container record-table-container'});
    let $div2 = $('<div>', {'class': 'row record-table-container-row'});
    let $div3 = $('<div>', {'class': 'col record-table-container-col', 'id': `response-${id}`}).html(response);
    let $div4 = $('<div>', {'class': 'col-3 text-right record-table-container-col-1'});

    let buttonClass = 'btn btn-link record-button';
    let $editButton = $('<button>', {'class': buttonClass}).html('<i class="far fa-edit" ></i>');
    $($editButton).click(() => edit_record(id, statement, response));

    buttonClass += ' record-delete-button';
    let $deleteButton = $('<button>', {'class': buttonClass}).html('<i class="fas fa-times"></i>');
    $($deleteButton).click(() => delete_record(id, statement));

    $($div4).prepend($deleteButton);
    $($div4).prepend($editButton);

    $($div2).prepend($div4);
    $($div2).prepend($div3);

    $($div1).prepend($div2);

    $($rCell).prepend($div1);

    $($row).prepend($sCell);
    $($row).append($rCell);
    $('#record-table-tbody').prepend($row);
}

function onCloseListener(){
    $('#add-record-modal').on('hidden.bs.modal', function (e) {
        $('#add-record-statement').val('');
        $('#add-record-response').val('');
        $('#add-record').off();
        $('.add-record-error-message').remove();
    })
    $('#edit-title-modal').on('hidden.bs.modal', function (e) {
        $('.edit-title-error-message').remove();
    })
}

function errorResponseTitle(xhr, errorClass){
    let errors = JSON.parse(xhr.responseText);
    errors = errors.error
    for(error of errors){
        let props = {'class': `${errorClass}-error-message alert alert-danger`, 'role': 'alert'}
        let $error_message = $('<div>', props);
        $error_message.text(error);
        $(`#${errorClass}-modal-body`).append($error_message);
    }
}

function edit_title(id, object_type){
    let new_name = $('#edit-title-new-name').val();
    var csrftoken = getToken();

    data = {
        'name': new_name,
        'object_id': id,
        'object_type': object_type
    }
    $.ajax({
        headers: { "X-CSRFToken": csrftoken },
        type: "POST",
        url: '/title/edit/',
        data: data,
        dataType: 'json',
        success: function(xhr){
            $('#page-title').html(new_name);
            $('#edit-title-modal').modal('hide');
        },
        error: function(xhr){
            errorResponseTitle(xhr, 'edit-title')
        }
    })
}
