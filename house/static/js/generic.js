function errorResponseTitle(xhr, errorClass){
    let errors = JSON.parse(xhr.responseText);
    errors = errors.error
    for(error of errors){
        let props = {'class': `${errorClass}-error-message alert alert-danger`, 'role': 'alert'}
        let $error_message = $('<div>', props);
        $error_message.text(error);
        $(`#${errorClass}-modal-body`).append($error_message);
    }
}

function getToken() {
    let name = 'csrftoken';
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
