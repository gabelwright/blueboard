from house.models import Room


def get_rooms(request):
    if request.user.is_authenticated:
        rooms = Room.objects.filter(owner=request.user).order_by('name').all()
        context = {'rooms': rooms}
        return context
    else:
        return {}
