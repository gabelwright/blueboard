from django import forms
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User

from house.models import House, Room, Device, Repair, Maintenance, MaintenanceEvent, Record, BaseName


generic_error = 'There was an error with your request. Please refresh the page and try again.'


class ContentTypeField(forms.Field):
    @staticmethod
    def get_content_type(value):
        object_types = {
            'house': House,
            'room': Room,
            'device': Device,
            'repair': Repair,
            'maintenance': Maintenance,
            'maintenanceEvent': MaintenanceEvent
        }
        return object_types.get(value)

    def to_python(self, value):
        return self.get_content_type(value)

    def validate(self, value):
        if not value:
            raise ValidationError(
                (generic_error),
                params={'value': value},
            )


class BaseObjectForm(forms.Form):
    name = forms.CharField(
        label='Name',
        widget=forms.TextInput,
        max_length=50,
        min_length=1)

    name.widget.attrs.update({
        'class': 'form-control'})

    date = forms.DateField(
        input_formats=['%m/%d/%Y'],
        widget=forms.DateInput(attrs={
            'class': 'form-control datetimepicker-input',
            'data-target': '#datetimepicker1'
        })
    )

    # date.widget.attrs.update({
    #     'class': 'input-group date col-sm-10',
    #     'id': 'datetimepicker1',
    #     'data-target-input': 'nearest'
    # })

    warrenty_length = forms.CharField(
        label='Warrenty Length',
        widget=forms.TextInput,
        max_length=150,
        min_length=1)

    warrenty_length.widget.attrs.update({
        'class': 'form-control',
        'placeholder': '1 Year'})

    cost = forms.IntegerField(
        label='Cost',
        widget=forms.NumberInput
    )

    cost.widget.attrs.update({
        'placeholder': '$100',
        'class': 'form-control'})

    notes = forms.CharField(
        label='Notes',
        widget=forms.Textarea,
        max_length=1000,
        min_length=1
    )

    notes.widget.attrs.update({
        'class': 'form-control',
        'placeholder': 'Additional notes....'})


class AddDeviceForm(BaseObjectForm):
    field_order = ['name', 'date', 'cost', 'purchase_from', 'warrenty_length', 'notes']

    purchase_from = forms.CharField(
        label='Store',
        widget=forms.TextInput)

    purchase_from.widget.attrs.update({
        'class': 'form-control',
        'placeholder': 'Amazon'})


class EditTitleForm(forms.ModelForm):
    object_type = ContentTypeField()
    object_id = forms.UUIDField()

    class Meta:
        model = BaseName
        fields = ['name', 'object_id', 'object_type']
        error_messages = {
            'name': {
                'max_length': ("Names are limited to 50 characters."),
                'required': 'A name is required.',
            },
            'object_id': {
                'invalid': generic_error
            }
        }


class AddRecordForm(forms.ModelForm):
    object_type = ContentTypeField()

    class Meta:
        model = Record
        fields = ['statement', 'response', 'object_id', 'object_type']
        error_messages = {
            'statement': {
                'max_length': ("Statments are limited to 150 characters."),
                'required': 'A statement is required for each record.',
            },
            'response': {
                'max_length': 'Responses are limited to 150 characters.'
            },
            'object_id': {
                'invalid': generic_error
            }
        }


class EditRecordForm(forms.ModelForm):
    record_id = forms.UUIDField()

    class Meta:
        model = Record
        fields = ['statement', 'response', 'record_id']
        error_messages = {
            'statement': {
                'max_length': ("Statments are limited to 150 characters."),
                'required': 'A statement is required for each record.',
            },
            'response': {
                'max_length': 'Responses are limited to 150 characters.'
            },
            'record_id': {
                'invalid': generic_error
            }
        }


class SignUpForm(forms.Form):
    def validate_password(value):
        if len(value) < 8:
            raise ValidationError(
                ('Password must be at least 8 characters long.'),
                params={'value': value},
            )

    def validate_email(email):
        e = User.objects.filter(email=email).exists()
        if e:
            raise ValidationError(
                ('That email has already been registered.'),
                params={'email': email},
            )

    email = forms.EmailField(
        label='Email',
        validators=[validate_email],
        widget=forms.EmailInput)

    password = forms.CharField(
        label='Password',
        max_length=100,
        validators=[validate_password],
        widget=forms.PasswordInput)

    email.widget.attrs.update({
        'class': 'form-control',
        'placeholder': 'Email'})
    password.widget.attrs.update({
        'class': 'form-control',
        'placeholder': 'Password'})

    def create_user(self):
        user = User.objects.create_user(
            username=self.cleaned_data['email'],
            email=self.cleaned_data['email'],
            password=self.cleaned_data['password'])
        House.objects.create(
            owner=user,
            name='Your House',
        )


class AuthForm(forms.Form):
    email = forms.CharField(
        label='Username/Email',
        widget=forms.TextInput)
    password = forms.CharField(
        label='Password',
        max_length=100,
        widget=forms.PasswordInput)

    email.widget.attrs.update({
        'class': 'form-control',
        'placeholder': 'Username/Email'})
    password.widget.attrs.update({
        'class': 'form-control',
        'placeholder': 'Password'})


class AddRoomForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = ['name']
        error_messages = {
            'statement': {
                'max_length': ("Room names are limited to 50 characters."),
                'required': 'A name is required for each room.',
            }
        }
