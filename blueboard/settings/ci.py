from .common import *

DEBUG = True

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'nice_marmot',
        'USER': 'runner',
        'PASSWORD': '',
        'HOST': 'postgres',
    }
}
